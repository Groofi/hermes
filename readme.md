<h1>Hermes</h1>

<h2>Intro</h2>
<p>
The backend and frontend are separated on the project.
Since this repository is dedicated to the backend, we will talk about it.
The project is made on a stack: <b>Django Rest Framework/Postgres/Celery/RabbitMQ/Docker/Gitlab Runner.</b>
</p>

<h2>Description</h2>
<p>
<b>Backend</b> it is used for inventory of books. <b>Django Rest Framework and Postgres</b> 
it is used to write to the database through the convenient Django interface by means of the 
DRF itself or its admin panel.
<br> 
<b>Celery/RabbitMQ</b> it is used to send dump databases to email for safer operation in cases
of unforeseen circumstances. Also, thanks to the rest api, we can work with the front in an excellent way,
which is presented in the full concept of the application.
<br>
<b>Docker и Gitlab Runner</b> for convenient automatic assembly.
</p>

<h2>Getting started</h2>
For a quick start, all you need is:

```docker compose up```

You will also need a secret key, which we will write to env file:

```django-insecure-v3r(5evqc(vh!)ce5ojck$^+^-k!a5*ewx=r^skpjyjo&yk!kw```

The env.example file is pretty clear in principle, it is mandatory to fill in Secret Key, Debug and
Postgres variables.