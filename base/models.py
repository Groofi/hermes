from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    REQUIRED_FIELDS = []

    first_name = None
    last_name = None
    last_login = None
    date_joined = None

    def get_full_name(self): ...

    def get_short_name(self): ...
