from django.apps import AppConfig
from django.contrib.auth.apps import AuthConfig


class GroupConfig(AuthConfig):
    verbose_name = "Группы"


class BaseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'base'
    verbose_name = 'Базовые сущности'
