FROM python:3.10

WORKDIR /app

COPY Pipfile Pipfile.lock /app/

RUN pip install pipenv

RUN pipenv install

COPY . /app/

RUN chmod 777 run_app.sh

EXPOSE 8000
