import os

from celery.schedules import crontab
from django.core.mail import EmailMessage

from hermes.settings.celery import app


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(crontab(minute=0, hour=0), get_dump.s(), name='task 10 second')


@app.task
def get_dump():
    dump_file = '/opt/dump_hermes.json'
    os.system(f'pipenv run python manage.py dumpdata --indent 4 > {dump_file}')
    email = os.environ.get('EMAIL_HOST_USER')
    if email:
        message = EmailMessage(
            'Dump',
            'Это файл с книгами',
            None,
            [email]
        )
        message.attach_file(dump_file)
        message.send()
