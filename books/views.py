from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, BasePermission, SAFE_METHODS

from books.models import Books, Photos, Boxes
from books.serializers import BooksSerializer, BooksListSerializer, PhotosSerializer, BoxesSerializer


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class BooksViewSet(viewsets.ModelViewSet):
    queryset = Books.objects.all()
    serializer_class = BooksSerializer
    permission_classes = [IsAdminUser | ReadOnly]

    def get_serializer_class(self):
        return (self.serializer_class, BooksListSerializer)[self.action == 'list']


class BoxesViewSet(viewsets.ModelViewSet):
    queryset = Boxes.objects.all()
    serializer_class = BoxesSerializer
    permission_classes = [IsAdminUser | ReadOnly]


class PhotosViewSet(viewsets.ModelViewSet):
    queryset = Photos.objects.all()
    serializer_class = PhotosSerializer
    permission_classes = [IsAdminUser]
