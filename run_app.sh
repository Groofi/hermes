pipenv run python manage.py migrate;
pipenv run python manage.py loaddata /opt/dump_hermes.json;
pipenv run python manage.py collectstatic --noinput;
pipenv run gunicorn hermes.wsgi:application --bind 0.0.0.0:8000 --workers 3 --log-level=info --access-logfile=/var/log/access.log --error-logfile=/var/log/error.log;
