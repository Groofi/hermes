from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import token_refresh, token_obtain_pair

from base.views import HomeView
from books.views import BooksViewSet, PhotosViewSet, BoxesViewSet
from hermes import settings

router = routers.DefaultRouter()
router.register('books', BooksViewSet, basename="books")
router.register('photos', PhotosViewSet, basename="photos")
router.register('boxes', BoxesViewSet, basename="boxes")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('', HomeView.as_view()),
    path('api-auth/', include('rest_framework.urls')),
    path('api/token/', token_obtain_pair, name='token_obtain_pair'),
    path('api/token/refresh/', token_refresh, name='token_refresh'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
